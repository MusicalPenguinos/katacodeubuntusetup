# Katacoda Ubuntu Setup #

This repo contains a short setup.sh script to create a user home directory and setup git config --global options for the katacoda ubuntu playground.

The playground can be found at [https://www.katacoda.com/courses/ubuntu/playground](https://www.katacoda.com/courses/ubuntu/playground)

### How do I get set up? ###

* Open the ubuntu playground environment using the link above.
* Clone this repository and run the setup script using the following commands:

&nbsp;
```bash
git clone git@bitbucket.org:MusicalPenguinos/katacodeubuntusetup.git
cd katacodeubuntusetup
source setup.sh
```
&nbsp;

* Then follow the instruction on the command line.
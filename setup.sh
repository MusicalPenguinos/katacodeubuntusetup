read -p "Enter username: " username
echo "Setting up user directory"
mkdir "/home/$username"
echo "HOME=/home/$username" >> /root/.bashrc
source /root/.bashrc
cd ~
read -p "Enter email to use for git: " email
read -p "Enter name to use for git: " name
git config --global user.email "$email"
git config --global user.name "$name"
git config --global push.default simple
echo "Setup complete:"
pwd
